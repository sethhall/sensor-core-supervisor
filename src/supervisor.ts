'use strict';

import fs from 'fs';
import { promises as fsp } from 'fs';
import path from 'path';
import express from 'express';
import { once } from 'events';
import forever from 'forever-monitor';

//import Server from "./server/index.js"

/*
const fs = require('fs');
const fsp = require('fs').promises;
const path = require('path');
const express = require('express');
//const forever = require('forever-monitor');
const { once } = require('events');
//const { server } = require( "./server/index.js")
*/


var rawdata = fs.readFileSync('corelight-softsensor.json');
var ss_config = JSON.parse(rawdata.toString());

var processes: forever.Monitor[] = [];

try { 
  fs.mkdirSync(ss_config.disk.directory);
  fs.mkdirSync(path.join(ss_config.disk.directory, 'supervisor'));
} catch (err) {
  if ( err.code != 'EEXIST' )
	console.log("Can't create the supervisor directory!");
}
//process.chdir(path.join(ss_config.disk.directory, 'supervisor'));

async function supervise_a_process(name: string, command: string[]) {
  const bin_dir = path.dirname(process.execPath);
  let zeekpath_ext = "";
  if ( process.env['CORELIGHT_ZEEKPATH_EXT'] )
    zeekpath_ext = process.env['CORELIGHT_ZEEKPATH_EXT'] + ":";

  zeekpath_ext += path.join(ss_config.disk.directory, 'zeek') + ":" +
                  path.normalize(path.join(bin_dir, '..', 'share', 'zeek'));

  let spool_dir = path.join(ss_config.disk.directory, 'spool', name);
  await make_a_dir(spool_dir);
  let proc = new (forever.Monitor)(command, {
    uid: name,
    cwd: spool_dir,
    silent: true,
    watch: false,
    max: 5,
    killSignal: 'SIGTERM',
    killTTL: 5000,
    env: {
      'CLUSTER_NODE':           name,
      'CORELIGHT_ZEEKPATH_EXT': zeekpath_ext,
    },
  });

  const cleanup = function(s: string) {
    //return s;
    let out = s.replace(/^\d{9,10}\.\d{4,6} /, '');
    out = out.replace(/((error|warning) in )?\<params\>, line 1: /, '');
    out = out.replace(/.*; line has invalid format.*$/, '');
    out = out.replace(/^.*::WRITER_CORELIGHT_[^:]+: /, '');
    out = out.replace(/\/Input::READER_CORELIGHT_[^:]+: /, ' :: ');
    out = out.replace(/\/Input::READER_CONFIG: /, ' :: ');
    out = out.replace(/.*\/Input::READER_ASCII: Init: /, '');
    out = out.replace(/^.*init-license.zeek, line .*: Licensed to.*$/, '');
    out = out.replace(/^listening on.*$/, '');
    out = out.replace(/(\[[[:digit:]]*\] )?[[:digit:]]{1,2}\/[[:digit:]]{1,2}\/[[:digit:]]{4} -- [[:digit:]]{1,2}:[[:digit:]]{1,2}:[[:digit:]]{1,2} - (\(.*:.*\) )?<.*> (-|\(.*\) --? ) ?/, '');
    out = out.replace(/.* memcap: .*/, '');
    out = out.replace(/.*cores online: .*/, '');
    return out;
  };
  proc.on('stderr', (data: string) => {
    data = cleanup(data.toString().trim());
    if ( ! /(^\s*$|stacktrace|libhilti|warning)/.test(data) )
      console.log(`${proc.uid}: ${data}`);
  });
  proc.on('stdout', (data: string) => {
    data = cleanup(data.toString().trim());
    if ( ! /(^[[:blank:]]*$|stacktrace|libhilti|warning)/.test(data) ) {
      console.log(`${proc.uid}: ${data}`);
    }
  });
  proc.on('exit', (data: string) => {
    console.log(`${proc.uid}: exited`);
  });
  proc.on('start', (data: string) => {
    console.log(`${proc.uid}: Started!`);
  });
  proc.start();
  await once(proc, 'start');

  processes.push(proc)
}

zeek.on('zeek_init', async () => {
  process.on('SIGINT', async () => {
  //process.on('SIGTERM', async () => {
    console.log("supervisor: shutting down");
    await Promise.all(processes.map(async (proc) => {
      try {
        let ee = proc.stop();
        console.log(`supervisor: Stopping process: ${proc.uid}`);
        await once(ee, 'exit');
      } catch (err) {
        console.log(`supervisor: Process not running or can't stop: ${proc.uid} (${err})`);
      }
    }));
    
    zeek.invoke("terminate");
  });
});


async function make_a_dir(dir: string) {
    try {
        await fsp.mkdir(dir);
    } catch(err) {
        if ( err.code != 'EEXIST' )
            console.log(`supervisor: Got some error while trying to make a directory?: ${err}`);
    }
}

async function setup_directory_structure(dir: string) {
    await make_a_dir(dir);
    await make_a_dir(path.join(dir, 'zeek'));
    await make_a_dir(path.join(dir, 'suricata'));
    await make_a_dir(path.join(dir, 'supervisor'));
    await make_a_dir(path.join(dir, 'spool'));
}

function how_many_workers(config): number {
    let number_of_workers = 0;
    config.sniff.map((elem) => {
        number_of_workers += elem.workers;
    });
    return number_of_workers;
}

async function write_config_files() {
    let number_of_workers = how_many_workers(ss_config);

    // Write the cluster layout file.
    let cluster_layout = zeek.invoke('Corelight::generate_cluster_layout', [number_of_workers]);
    await fsp.writeFile(path.join(ss_config.disk.directory, 'zeek', 'cluster-layout.zeek'), cluster_layout);   

    // Write the dynamic_script.zeek file
    await fsp.writeFile(path.join(ss_config.disk.directory, 'zeek', 'dynamic-script.zeek'), `@load /builtin/corelight/\n@load corelight/standard\nredef Corelight::disk_space="${ss_config.disk.directory}";`);

    // Write suricata.yaml
}

export async function stop_zeek() {
}

export async function start_suricata() {
  const bin_dir = path.dirname(process.execPath);
  if ( ss_config.suricata.enable ) {
    let suricata_config = path.join(ss_config.disk.directory, 'suricata', 'suricata.yaml');
    await supervise_a_process('suricata', [path.join(bin_dir, 'suricata'), '-c', suricata_config, '--af-packet', '--']);
    console.log("Started up Suricata!");
  }
}
export async function start_zeek() {
  await supervise_a_process('logger', [process.execPath, '--', path.join(ss_config.disk.directory, 'zeek', 'dynamic-script.zeek')]);
  await supervise_a_process('proxy-1', [process.execPath, '--', path.join(ss_config.disk.directory, 'zeek', 'dynamic-script.zeek')]);
  await supervise_a_process('manager', [process.execPath, '--', path.join(ss_config.disk.directory, 'zeek', 'dynamic-script.zeek')]);

  let interface_offset = 0;
  await Promise.all(ss_config.sniff.map(async (sniff, interface_offset) => {
    let i = 0;
    while ( i < sniff.workers ) {
      console.log(`starting worker-${i} .. ${sniff.interface}`);
      ++i;
      let padded_worker_name = "worker-" + i.toString().padStart(2, "0");
      await supervise_a_process(padded_worker_name, [process.execPath, '-i', `af_packet::${sniff.interface}`, `AF_Packet::fanout_id=${110+interface_offset}`, '--', path.join(ss_config.disk.directory, 'zeek', 'dynamic-script.zeek')]);
    }
  }));
  console.log("Started up Zeek Cluster!");
}


// This is a hack to keep the event loop running.
function tick() {
    setTimeout(tick, 500);
}
setTimeout(tick, 5);


export async function restart_it() {
  console.log("Restarting everything");
  await Promise.all(processes.map(async (proc) => {
    try {
      let ee = proc.restart();
      await once(ee, 'restart');
    } catch (err) {
      console.log(`supervisor: Process not running or can't stop: ${proc.uid} (${err})`);
    }
  }));
  console.log("Done restarting the Software Sensor");
}

export async function shut_it() {
  await Promise.all(processes.map(async (proc) => {
    try {
        let ee = proc.stop();
        console.log(`supervisor: Stopping process: ${proc.uid}`);
        await once(ee, 'exit');
      } catch (err) {
        console.log(`supervisor: Process not running or can't stop: ${proc.uid} (${err})`);
      }
    }));
}

export async function start() {

zeek.on('Corelight::config_is_fully_updated', async () => {
  //console.log("config is fully updated");

  await setup_directory_structure(ss_config.disk.directory);
  await write_config_files();

  await start_suricata();
  await start_zeek();
});


const app = express();
const port = 3000;
app.get('/restart', async (req, res) => {
  console.log("Restarting everything");
  await Promise.all(processes.map(async (proc) => {
    try {
      let ee = proc.restart();
      await once(ee, 'restart');
    } catch (err) {
      console.log(`supervisor: Process not running or can't stop: ${proc.uid} (${err})`);
    }
  }));
  console.log("Done restarting the Software Sensor");
  res.send('OK');
});
app.get('/shutdown', async (req, res) => {
  await Promise.all(processes.map(async (proc) => {
    try {
        let ee = proc.stop();
        console.log(`supervisor: Stopping process: ${proc.uid}`);
        await once(ee, 'exit');
      } catch (err) {
        console.log(`supervisor: Process not running or can't stop: ${proc.uid} (${err})`);
      }
    }));

    res.send('OK');
    //zeek.invoke("terminate");
});




var { graphqlHTTP } = require('express-graphql');
var { buildSchema } = require('graphql');

// Construct a schema, using GraphQL schema language
var schema = buildSchema(`
  type Query {
    restart: String
    shutdown: String
  }
`);

// The root provides a resolver function for each API endpoint
var root = {
  restart: async function() {
    await restart_it();
    return 'system restarted!!!';
  },
  shutdown: async function() {
    await shut_it();
    return 'system shutdown!!!'
  }
};

app.use(express.static('ui/build'));
app.use('/graphql', graphqlHTTP({
  schema: schema,
  rootValue: root,
  graphiql: true,
}));

app.listen(port, () => {
  console.log(`Software Sensor API listening on port ${port}`)
});

}
