import { Command } from 'commander';
const program = new Command();

import * as supervisor from './supervisor';

const helpHeader = `\
 ██████╗ ██████╗ ██████╗ ███████╗██╗     ██╗ ██████╗ ██╗  ██╗████████╗
██╔════╝██╔═══██╗██╔══██╗██╔════╝██║     ██║██╔════╝ ██║  ██║╚══██╔══╝
██║     ██║   ██║██████╔╝█████╗  ██║     ██║██║  ███╗███████║   ██║
██║     ██║   ██║██╔══██╗██╔══╝  ██║     ██║██║   ██║██╔══██║   ██║
╚██████╗╚██████╔╝██║  ██║███████╗███████╗██║╚██████╔╝██║  ██║   ██║
 ╚═════╝ ╚═════╝ ╚═╝  ╚═╝╚══════╝╚══════╝╚═╝ ╚═════╝ ╚═╝  ╚═╝   ╚═╝
This is a Corelight Software Sensor. It makes network monitoring easy.
Copyright 2020-2022 Corelight, Inc.`

program
  .description(helpHeader)
  .option('-c, --config <file>', 'Software Sensor config file')

program
  .command('start')
  .description('Start up the Software Sensor')
  .action(async (source, destination) => {
    await supervisor.start();
  });

program
  .command('versions')
  .description('Output versions of all included libraries')
  .action(async (source, destination) => {
    console.log(zeek.invoke('Corelight::read_file', ['/builtin/versions']));
    process.exit(0);
  })

program
  .command('licenses')
  .description('Output licenses of all included libraries')
  .action(async (source, destination) => {
    console.log(zeek.invoke('Corelight::read_file', ['/builtin/licenses']));
    process.exit(0);
  })

program
  .command('license')
  .description('Display your loaded license')
  .action(async (source, destination) => {
    console.log('license command called');
    zeek.on('Corelight::license_loaded', function(l) {
      let license = zeek.invoke('Corelight::get_license_payload', []);
      console.log(license);
      process.exit(0);
    });
  });

program
  .parse(['zeek', 'corelight-softsensor'].concat(zeek.global_vars['zeek_script_args']))

