/* eslint-disable */
// import Axios from 'axios';
// import { pathOr } from 'ramda';
// import { store } from '../ducks';
// import { login, setIdaptiveToken } from '../ducks/auth';


export const env = process.env.REACT_APP_ENV || 'dev';

console.log(process.env.REACT_APP_ENV);
console.log(env);
let baseURL = `http://localhost:4000`;

// apollo

import {
  ApolloClient,
  createHttpLink,
  InMemoryCache,
  operationName,
} from '@apollo/client';
import { setContext } from '@apollo/client/link/context';
// import { authStore } from '../../ducks'
import { onError } from 'apollo-link-error';
import { ApolloLink } from 'apollo-link';

import { RetryLink } from 'apollo-link-retry';
/*import {
  loadingGroupAdd,
  loadingGroupClear,
  loadingGroupDone,
} from '../ducks/networking';*/

const httpLink = createHttpLink({
  uri: "/graphql",
});

const errorLink = onError(({ graphQLErrors, networkError, operation }) => {
  console.log(operation);
  console.log(networkError);
  if (graphQLErrors)
    graphQLErrors.forEach(({ message, locations, path }) =>
      console.log(
        '[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}',
      ),
    );
  if (networkError) {
    console.log(`[Network error]: ${networkError}`);
    document.cookie.split(';').forEach(function (c) {
      document.cookie = c
        .replace(/^ +/, '')
        .replace(/=.*/, '=;expires=' + new Date().toUTCString() + ';path=/');
    });
  }

  // store.dispatch(loadingGroupClear());
});

const authLink = setContext((_, { headers }, ...rest) => {
  console.log(headers, rest);
  // const token = pathOr(false, ['Auth', 'token'], store.getState());
  // return the headers to the context so httpLink can read them
  return {
    headers: {
      ...headers,
      // authorization: token ? `Bearer ${token}` : '',
    },
  };
});

const interceptorMiddleware = new ApolloLink((operation, forward) => {
  // store.dispatch(loadingGroupAdd(operation.operationName));
  return forward(operation);
});

const interceptorAfterware = new ApolloLink((operation, forward) => {
  return forward(operation).map((response) => {
    const {
      response: { headers },
    } = operation.getContext();

    // let empty = isEmpty(headers);
    // if (!empty) {
    //   const token = headers.getToken("x-token")
    //
    //   if (!!token) {
    //     store.dispatch(login(jwt))
    //   }
    // }
    // store.dispatch(loadingGroupDone(operation.operationName));
    return response;
  });
});

const retryLink = new RetryLink();

const link = ApolloLink.from([
  interceptorAfterware,
  interceptorMiddleware,
  errorLink,
  retryLink,
  authLink,
  httpLink,
]);

export const client = new ApolloClient({
  link: link,
  cache: new InMemoryCache(),
});
