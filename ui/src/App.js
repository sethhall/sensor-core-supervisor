import React from 'react';
import {
  ChakraProvider,
  Box,
  Text,
  VStack,
  Grid,
  theme,
} from '@chakra-ui/react';
import { ColorModeSwitcher } from './ColorModeSwitcher';

import { ApolloProvider } from '@apollo/client';
import { client } from './networking';
import Buttons from "./Buttons";


const App = () => {

  return (
    <ChakraProvider theme={theme}>
      <ApolloProvider client={client}>
        <Box textAlign="center" fontSize="xl">
          <Grid minH="100vh" p={3}>
            <ColorModeSwitcher justifySelf="flex-end"/>
            <VStack spacing={8}>
              <Text>Corelight Software Sensor</Text>
              <Buttons/>
            </VStack>
          </Grid>
        </Box>
      </ApolloProvider>
    </ChakraProvider>
  );
}

export default App;

