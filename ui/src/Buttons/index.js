import React, { useEffect } from 'react'
import { Button,  useToast, VStack } from "@chakra-ui/react";
import { gql, useLazyQuery } from "@apollo/client";



const RESTART = gql`
query RESTART{
 restart
}
`

const SHUTDOWN = gql`
query SHUTDOWN{
 shutdown
}
`
function Buttons () {

  const [restart, { rloading, rerror, rdata }] = useLazyQuery(RESTART);
  const [shutdown, { sloading, serror, sdata }] = useLazyQuery(SHUTDOWN)

  const toast = useToast()

  const handleRestart = () => {
    restart().then(r =>{
      toast({
        title: 'Status',
        description: r.data.restart,
        status: 'success',
        duration: 9000,
        isClosable: true,
      })
    } )
  }


  const handleShutdown = () => {
    shutdown().then(r => {
      toast({
        title: 'Status',
        description: r.data.shutdown,
        status: 'success',
        duration: 9000,
        isClosable: true,
      })
    })
  }

  return (
    <VStack spacing={8}>
      <Button onClick={handleRestart}>Restart</Button>
      <Button onClick={handleShutdown}>Shutdown</Button>
    </VStack>
  )
}

export default Buttons